# Resources
---
## Android
* [Android Development Getting Started](http://developer.android.com/training/index.html)

## Xtend
* http://www.eclipse.org/xtend/documentation.html

## Genemotion / virtual box (Fast android emulator)
* https://cloud.genymotion.com/page/doc/#collapse8

## Miscellaneous
* [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)